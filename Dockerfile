FROM python:latest
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt --no-cache-dir
COPY . /code/
ENV FLASK_APP main.py
ENV FLASK_DEBUG=1
CMD flask run --host=0.0.0.0