# Social [ Flask-MySQL-Docker]

## How to run
1. Clone the repository

    ```
    git clone origin https://gitlab.com/sajidkhan/social.git
    ```
2. Enter the ```social``` directory

    ```
    cd social
    ```
3. Run ```docker-compose up --build```

    ```
    docker-compose up --build
    ```
4. Browse this url : ``` http://localhost:5000/pythonlogin/ ```
